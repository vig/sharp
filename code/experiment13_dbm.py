import argparse
import os
from colorsys import rgb_to_hsv
from typing import Callable, Union

import matplotlib.pyplot as plt
import numpy as np
from matplotlib.axes import Axes
from MulticoreTSNE import MulticoreTSNE as TSNE
from PIL import Image
from sklearn import inspection
from sklearn.base import BaseEstimator, ClassifierMixin
from sklearn.manifold import Isomap
from sklearn.model_selection import train_test_split
from sklearn.neighbors import KNeighborsClassifier
from sklearn.neural_network import MLPClassifier
from umap import UMAP

import ae
import colors
import ssnp
import sharp

BoundingBox = tuple[float, float, float, float]

cmap = colors.CMAP_10_OLD


def get_bounding_box(X_proj: np.ndarray) -> tuple[float, float, float, float]:
    x_min, y_min = X_proj.min(axis=0)
    x_max, y_max = X_proj.max(axis=0)

    return x_min, x_max, y_min, y_max


def make_grid(
    x_min: float, x_max: float, y_min: float, y_max: float, side_length: int
) -> np.ndarray:
    xx, yy = np.meshgrid(
        np.linspace(x_min, x_max, side_length), np.linspace(y_min, y_max, side_length)
    )

    return np.c_[xx.ravel(), yy.ravel()]


def entropy(probs: np.ndarray) -> float:
    nonzero_probs = probs[probs.nonzero()]
    return -np.sum(nonzero_probs * np.log(nonzero_probs))


def normalized_entropy(probs: np.ndarray) -> float:
    if len(probs) == 1:
        prob = probs[0]
        if np.isclose(min(prob, 1.0 - prob), 0.0):
            return 0.0
        return -(1 / np.log(2)) * (prob * np.log(prob) + (1 - prob) * np.log(1 - prob))
    return entropy(probs) / np.log(len(probs))


def batch_normalized_entropy(probs: np.ndarray) -> np.ndarray:
    return np.apply_along_axis(normalized_entropy, -1, probs)


def dbm_for_estimator(
    model: MLPClassifier,
    inverter: Union[sharp.ShaRP, ssnp.SSNP],
    bounding_box: BoundingBox,
    grid_res: int,
    ax: Axes,
    cmap=cmap,
):
    # inspection.DecisionBoundaryDisplay.from_estimator(
    #     model,
    #     proj_data,
    #     response_method="predict",
    #     plot_method="pcolormesh",
    #     grid_resolution=grid_res,
    #     ax=ax,
    #     cmap=cmap,
    # )
    grid = make_grid(*bounding_box, grid_res)
    classes = model.predict(inverter.inverse_transform(grid)).astype(np.uint8)

    cmapped = cmap(classes)
    ax.imshow(
        cmapped.reshape((grid_res, grid_res, 4)),
        origin="lower",
        interpolation="none",
        resample=False,
    )
    ax.axis("off")


def gen_confusion_dbm(
    estimator: MLPClassifier,
    inverter: Union[sharp.ShaRP, ssnp.SSNP],
    bounding_box: BoundingBox,
    grid_res: int,
    ax: Axes,
    confusion_alpha=1.3,
    cmap=cmap,
):
    grid = make_grid(*bounding_box, grid_res)
    probs = estimator.predict_proba(inverter.inverse_transform(grid))
    classes = estimator.classes_[np.argmax(probs, axis=1)]
    # certainty = 1.0 - confusion**confusion_alpha
    certainty = probs.max(axis=1) ** confusion_alpha

    classes = classes.astype(np.uint8)
    hsv = [rgb_to_hsv(*cmap(cl)[:3]) for cl in classes]
    confused_hsv = [(h, c * s, v) for (h, s, v), c in zip(hsv, certainty)]
    img = Image.fromarray(
        (np.reshape(confused_hsv, (grid_res, grid_res, 3)) * 255).astype(np.uint8),
        mode="HSV",
    )
    ax.imshow(img, origin="lower", interpolation="none", resample=False)
    ax.axis("off")


def gen_and_save_dbm(
    X_2d: np.ndarray,
    classifier: ClassifierMixin,
    inverter: Union[sharp.ShaRP, ssnp.SSNP],
    output_dir: str,
    grid_res: int,
    dataset_name: str,
    alg_name: str,
):
    fig, ax = plt.subplots(figsize=(20, 20))
    dbm_for_estimator(
        classifier,
        inverter,
        get_bounding_box(X_2d),
        grid_res=grid_res,
        ax=ax,
        cmap=cmap if len(classifier.classes_) <= 10 else plt.get_cmap("tab20"),
    )
    fig.savefig(
        fname := os.path.join(output_dir, f"{dataset_name}_{alg_name}.png"),
        bbox_inches="tight",
        pad_inches=0.0,
    )
    print(fname)
    plt.close(fig)


def gen_and_save_confusion_dbm(
    X_2d: np.ndarray,
    classifier: ClassifierMixin,
    inverter: Union[sharp.ShaRP, ssnp.SSNP],
    output_dir: str,
    grid_res: int,
    dataset_name: str,
    alg_name: str,
):
    fig, ax = plt.subplots(figsize=(20, 20))
    gen_confusion_dbm(
        classifier,
        inverter,
        get_bounding_box(X_2d),
        grid_res=grid_res,
        ax=ax,
        cmap=cmap if len(classifier.classes_) <= 10 else plt.get_cmap("tab20"),
    )
    fig.tight_layout()
    fig.savefig(
        fname := os.path.join(output_dir, f"{dataset_name}_{alg_name}_Confusion.png"),
        bbox_inches="tight",
        pad_inches=0.0,
    )
    print(fname)

    plt.close(fig)


def plot(X, y, figname=None):
    if len(np.unique(y)) <= 10:
        cmap = colors.CMAP_10_OLD
    else:
        cmap = plt.get_cmap("tab20")

    fig, ax = plt.subplots(figsize=(20, 20))

    for cl in np.unique(y):
        ax.scatter(X[y == cl, 0], X[y == cl, 1], c=[cmap(cl)], label=cl, s=375)
        ax.axis("off")

    if figname is not None:
        fig.savefig(figname)

    plt.close("all")


def main():
    plt.rcParams.update(
        {
            "savefig.dpi": 256,
            "savefig.bbox": "tight",
            "savefig.pad_inches": 0.0,
            "lines.markersize": 2.0,
        }
    )

    parser = argparse.ArgumentParser("experiment_13")
    parser.add_argument("--output-dir", type=str, default="results_dbm")
    parser.add_argument(
        "--datasets",
        nargs="*",
        default=["mnist", "fashionmnist", "har", "reuters", "usps"],
    )
    parser.add_argument("--verbose", "-v", action="store_true")
    parser.add_argument("--epochs", "-e", type=int, default=20)
    parser.add_argument("--grid_res", "-g", type=int, default=300)

    args = parser.parse_args()

    output_dir = args.output_dir
    print(f"Outputtting data to {output_dir}")
    if not os.path.exists(output_dir):
        os.makedirs(output_dir)
    verbose = args.verbose

    data_dir = "../data"
    data_dirs = args.datasets
    print(data_dirs)

    epochs = args.epochs
    grid_res = args.grid_res

    for d in data_dirs:
        dataset_name = d

        X = np.load(os.path.join(data_dir, d, "X.npy"))
        y = np.load(os.path.join(data_dir, d, "y.npy"))
        print("------------------------------------------------------")
        print("Dataset: {0}".format(dataset_name))
        print(X.shape)
        print(y.shape)
        print(np.unique(y))

        X_train, _, y_train, _ = train_test_split(
            X,
            y,
            train_size=min(5_000, int(0.9 * X.shape[0])),
            random_state=420,
            stratify=y,
        )

        def make_and_fit_mlp(X, y) -> MLPClassifier:
            return MLPClassifier(
                hidden_layer_sizes=(512, 128, 32),
                activation="relu",
                max_iter=100,
                random_state=420,
            ).fit(X, y)

        clf = make_and_fit_mlp(X_train, y_train)
        sharp_gt = sharp.ShaRP(
            X.shape[1],
            len(np.unique(y_train)),
            variational_layer="diagonal_normal",
            variational_layer_kwargs=dict(kl_weight=0.05),
            bottleneck_activation="linear",
            bottleneck_l1=0.0,
            bottleneck_l2=0.5,
        )
        sharp_gt.fit(X_train, y_train, epochs=epochs, verbose=verbose, batch_size=64)
        X_sharp_gt = sharp_gt.transform(X_train)
        plot(
            X_sharp_gt,
            y_train,
            os.path.join(output_dir, f"{dataset_name}_ShaRP-GT_Proj.png"),
        )

        gen_and_save_dbm(
            X_sharp_gt, clf, sharp_gt, output_dir, grid_res, dataset_name, "ShaRP-GT"
        )
        gen_and_save_confusion_dbm(
            X_sharp_gt, clf, sharp_gt, output_dir, grid_res, dataset_name, "ShaRP-GT"
        )

        ssnp_gt = ssnp.SSNP(
            epochs=epochs,
            verbose=verbose,
            patience=0,
            opt="adam",
            bottleneck_activation="linear",
        )
        ssnp_gt.fit(X_train, y_train)
        X_ssnp_gt = ssnp_gt.transform(X_train)
        plot(
            X_ssnp_gt,
            y_train,
            os.path.join(output_dir, f"{dataset_name}_SSNP-GT_Proj.png"),
        )
        gen_and_save_dbm(
            X_ssnp_gt, clf, ssnp_gt, output_dir, grid_res, dataset_name, "SSNP-GT"
        )
        gen_and_save_confusion_dbm(
            X_ssnp_gt, clf, ssnp_gt, output_dir, grid_res, dataset_name, "SSNP-GT"
        )

        # tsne = TSNE(n_jobs=4)
        # X_tsne = tsne.fit_transform(X_train)
        # tsne_knn = make_and_fit_knn(X_tsne)
        # gen_and_save_dbm(X_tsne, tsne_knn, output_dir, grid_res, dataset_name, "TSNE")

        # ump = UMAP(random_state=420)
        # X_umap = ump.fit_transform(X_train)
        # umap_knn = make_and_fit_knn(X_umap)
        # gen_and_save_dbm(X_umap, umap_knn, output_dir, grid_res, dataset_name, "UMAP")

        # aep = ae.AutoencoderProjection(epochs=epochs, verbose=0)
        # aep.fit(X_train)
        # X_aep = aep.transform(X_train)
        # aep_knn = make_and_fit_knn(X_aep)
        # gen_and_save_dbm(X_aep, aep_knn, output_dir, grid_res, dataset_name, "AE")

        # isomap = Isomap().fit(X_train)
        # X_isomap = isomap.transform(X_train)
        # isomap_knn = make_and_fit_knn(X_isomap)
        # gen_and_save_dbm(X_isomap, isomap_knn, output_dir, grid_res, dataset_name, "ISOMAP")


if __name__ == "__main__":
    main()
