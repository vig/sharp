import argparse
from datetime import datetime
import pathlib
from itertools import starmap
import pandas as pd

import colors
import matplotlib.pyplot as plt
import metrics
import numpy as np
import plotly.express as px
import radial
import tensorflow as tf
from keras import callbacks
from sharp import ShaRP, compute_all_metrics
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import LabelBinarizer, minmax_scale


def setup_figure():
    fig, ax = plt.subplots(1, 1, subplot_kw={"aspect": "equal"}, figsize=(8, 8), dpi=256)
    ax.axis("off")
    ax.set_autoscale_on(False)
    ax.set_ylim(0.0, 1.0)
    ax.set_xlim(0.0, 1.0)
    return fig, ax


def gen_plotly_plots(X, X_proj, y, y_key, base_path=pathlib.Path(".")):
    cmap = colors.CMAP_10_OLD

    X_proj = X_proj.copy()
    X = X.copy()
    y = y.copy()

    def to_css(r: int, g: int, b: int, _a: int) -> str:
        return f"#{r:02x}{g:02x}{b:02x}"

    css_colors = list(starmap(to_css, [cmap(i, bytes=True) for i in range(10)]))

    # So that they Y levels show up in order on the legend.
    sorted_idxs = np.argsort(y)

    X_proj = X_proj[sorted_idxs, :]
    X = X_proj[sorted_idxs, :]
    y = y[sorted_idxs]

    fig = px.scatter_3d(
        x=X_proj[:, 0],
        y=X_proj[:, 1],
        z=X_proj[:, 2],
        color=y.astype(str),
        color_discrete_sequence=css_colors,
        hover_data={
            "x": X_proj[:, 0],
            "y": X_proj[:, 1],
            "z": X_proj[:, 2],
            "color": y,
            "class": [y_key[lbl] for lbl in y],
        },
    )
    fig.update_layout(
        {
            "scene": {
                "xaxis": {"visible": False},
                "yaxis": {"visible": False},
                "zaxis": {"visible": False},
            }
        }
    )
    fig.write_html(base_path / "plotly_sharp.html")
    np.save(base_path / "X_sharp_gt.npy", X_proj)

    from MulticoreTSNE import MulticoreTSNE as TSNE

    X_tsne_2d = TSNE(n_jobs=8).fit_transform(X)
    fig = px.scatter(
        x=X_tsne_2d[:, 0],
        y=X_tsne_2d[:, 1],
        color=y.astype(str),
        color_discrete_sequence=css_colors,
        hover_data={
            "x": X_tsne_2d[:, 0],
            "y": X_tsne_2d[:, 1],
            "color": y,
            "class": [y_key[lbl] for lbl in y],
        },
    )
    fig.update_yaxes(scaleanchor="x", scaleratio=1)
    fig.write_html(base_path / "plotly_tsne_2d.html")
    X_tsne_2d = minmax_scale(X_tsne_2d)
    fig, ax = setup_figure()
    ax.scatter(*X_tsne_2d.T, c=y, cmap=colors.CMAP_10_OLD)
    fig.savefig(base_path / "tsne_2d.png")
    plt.close(fig)
    np.save(base_path / "X_tsne_2d.npy", X_tsne_2d)

    X_tsne_3d = TSNE(n_components=3, n_jobs=8).fit_transform(X)
    fig = px.scatter_3d(
        x=X_tsne_3d[:, 0],
        y=X_tsne_3d[:, 1],
        z=X_tsne_3d[:, 2],
        color=y.astype(str),
        color_discrete_sequence=css_colors,
        hover_data={
            "x": X_tsne_3d[:, 0],
            "y": X_tsne_3d[:, 1],
            "z": X_tsne_3d[:, 2],
            "color": y,
            "class": [y_key[lbl] for lbl in y],
        },
    )
    fig.update_yaxes(scaleanchor="x", scaleratio=1)
    fig.write_html(base_path / "plotly_tsne_3d.html")
    np.save(base_path / "X_tsne_3d.npy", X_tsne_3d)


def main():
    # tf.debugging.enable_check_numerics()
    # tf.data.experimental.enable_debug_mode()

    plt.rcParams.update(
        {
            "savefig.dpi": 256,
            "savefig.bbox": "tight",
            "savefig.pad_inches": 0.0,
            "lines.markersize": 2.0,
        }
    )
    parser = argparse.ArgumentParser()
    parser.add_argument("--output-dir", type=str, default="results_spherical")
    parser.add_argument(
        "--datasets",
        nargs="+",
        default=["mnist", "fashionmnist", "har", "reuters", "usps", "quickdraw2"],
    )
    parser.add_argument("--verbose", "-v", action="store_true")
    parser.add_argument("--epochs", "-e", type=int, default=100)
    parser.add_argument("--full-debug", action="store_true", default=False)

    args = parser.parse_args()

    if args.full_debug:
        tf.debugging.experimental.enable_dump_debug_info(
            "./tmp/tfdb2_logdir", tensor_debug_mode="FULL_HEALTH", circular_buffer_size=-1
        )

    output_dir = pathlib.Path(args.output_dir)

    if not output_dir.exists():
        output_dir.mkdir()
    data_dir = pathlib.Path(pathlib.Path(__file__).parent / "../data")
    data_dirs: list[str] = args.datasets
    print(data_dirs)

    epochs = args.epochs

    results = []

    for d in data_dirs:
        dataset_name = d

        (output_dir / d).mkdir(exist_ok=True)

        X = np.load(data_dir / d / "X.npy")
        y = np.load(data_dir / d / "y.npy")
        if (fname := data_dir / d / "y_key.json").exists():
            import json

            with open(fname) as f:
                y_key = json.load(f)
            y_key = {int(k): v for k, v in y_key.items()}
        else:
            y_key = {i: str(i) for i in np.unique(y)}
        print("------------------------------------------------------")
        print("Dataset: {0}".format(dataset_name))
        print(X.shape)
        print(y.shape)
        print(np.unique(y))

        X_train, _, y_train, _ = train_test_split(
            X, y, train_size=min(5_000, int(0.9 * X.shape[0])), random_state=420, stratify=y
        )
        label_bin = LabelBinarizer().fit(y_train)
        train_dataset = tf.data.Dataset.from_tensor_slices(
            (X_train, (label_bin.transform(y_train), X_train))
        ).shuffle(buffer_size=1024)
        val_dataset = train_dataset.take(500).batch(32)
        train_dataset = train_dataset.skip(500).take(30000).batch(64)

        sharp_gt = ShaRP(
            X.shape[1],
            len(np.unique(y_train)),
            variational_layer="spherical",
            variational_layer_kwargs={},
            latent_dim=3,
            var_leaky_relu_alpha=-0.0001,
            bottleneck_activation="linear",
            bottleneck_l1=0.0,
            bottleneck_l2=0.5,
        )

        sharp_gt.fit(
            train_dataset,
            validation_data=val_dataset,
            epochs=epochs,
            verbose=args.verbose,
            callbacks=[
                callbacks.EarlyStopping(
                    monitor="loss",
                    min_delta=1e-3,
                    patience=20,
                    restore_best_weights=True,
                    mode="min",
                ),
                callbacks.TensorBoard(
                    "logs/fit/" + f"{datetime.now():%d-%m-%Y-%H-%M-%S}", histogram_freq=1
                ),
            ],
        )

        X_i = sharp_gt.transform(X_train)

        gen_plotly_plots(
            X_train,
            X_i,
            y_train,
            y_key,
            output_dir / d,
        )

        D_high = metrics.compute_distance_list(X_train)
        D_low = radial.fast_pairwise_arc_length_distance(X_i)

        metric_values = compute_all_metrics(
            X_train,
            X_i,
            D_high,
            D_low,
            y_train,
            sharp_gt.inverse_transform(sharp_gt.transform(X_train)),
        )
        results.append((dataset_name, "ShaRP-GT") + metric_values)

    df = pd.DataFrame(
        results,
        columns=[
            "dataset_name",
            "alg",
            "T_train",
            "C_train",
            "R_train",
            "S_train",
            "N_train",
            "DSC_train",
            "CC_train",
            "MSE_train",
            "MSE_test",
        ],
    )
    df.to_csv(output_dir / "metrics.csv", header=True, index=None)


if __name__ == "__main__":
    main()
